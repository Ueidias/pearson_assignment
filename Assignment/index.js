var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var app = express();
app.use(cors());
app.use(express.static(__dirname+"/public"));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
module.exports = app;

var port = process.env.PORT || 8700;

//Firing up the client's server
app.listen(port,function(){
    console.log("App is listening on "+port);
});

//Sending the index file to the customer's browser when a GET request is received
app.get('/',function(req,res){
    res.sendFile('public/app/views/index.html',{root:'.'});
});
