angular.module('userFormController', [])

    .controller('userFormCtrl', ['userForm', '$timeout', '$location', '$window', function (userForm, $timeout, $location, $window) {
        const app = this;

        var UserItemList = [];
        app.UserRetrievedList = [];

        app.pushToTable = function () {
            var userID = app.userID;
            var userName = app.userName;
            var userAddress = app.userAddress;
           UserItemList.push({
                "userItemID": userID,
                "userItemName": userName,
                "userItemAddress": userAddress
            });
            localStorage.setItem("UserItemList", JSON.stringify(UserItemList));
            app.UserRetrievedList = JSON.parse(localStorage.getItem("UserItemList"));
        };
        app.addUser = function () {
            var userID = app.userID;
            var userName = app.userName;
            var userAddress = app.userAddress;
            app.UserInsertionBody = {
                "userID": userID,
                "userName": userName,
                "userAddress": userAddress           
                };
            userForm.insertUserData(app.UserInsertionBody).then(function (res) {
                    $window.alert("New user was added successfully");
                })
        };

        app.addUser1 = function (body) {
            var userID = body.userItemID;
            var userName = body.userItemName;
            var userAddress = body.userItemAddress;
            app.UserInsertionBody = {
                "userItemID": userID,
                "userItemName": userName,
                "userItemAddress": userAddress
                };
            userForm.insertUserData(app.UserInsertionBody).then(function (res) {
                    $window.alert("New user was added successfully");
                })
        };

        app.addAllUsers = function () {
            app.UserRetrievedList = JSON.parse(localStorage.getItem("UserItemList")); 
            app.UserInsertionBody = {
                "userItemList": app.UserRetrievedList
            };
            userForm.insertAllUserData(app.UserInsertionBody).then(function (res) {
                    $window.alert("User List added successfully");
                })
        };
    }]);