'use strict'
// Main application of the client
// this has all the dependencies of other modules
angular.module('Assignment', [
    'Assignment.routes',
    'userFormController',
    'userFormService'
]);

