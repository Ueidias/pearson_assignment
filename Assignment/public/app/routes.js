'use strict';

// setting all the routes of the Single page application client using AngularJS ui-router
angular.module('Assignment.routes',['ui.router'])
    .config(function($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/dashboard/userForm');
        $stateProvider
            .state('home', {
                url: '/home',
                templateUrl: 'app/views/pages/home.html',
                authenticated: false
            })
            .state('dashboard',{
                url:'/dashboard',
                templateUrl:'app/views/pages/dashboard.html',
                controller:'userFormCtrl as userForm',
                authenticated:true
            })
            .state('dashboard.userForm',{
                url:'/userForm',
                templateUrl:'app/views/pages/SidebarForms/userForm.html',
                controller:'userFormCtrl as userForm',
                authenticated:true
            })
    });