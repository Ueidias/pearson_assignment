var schema = require('../db.schema');
var usermodel = schema.model('user');
var usermodel1 = schema.model('user1');

// To add new users
exports.addUsers = function (userBody) {
    return new Promise(function (resolve, reject) {
        var newUser = new usermodel({
            userItemList:userBody.userItemList
        });
        newUser.save().then(function () {
            resolve({ status: 201, message: 'user details added successfully', data: newUser });
        }).catch(function (reason) {
            reject({ status: 500, message: "Error" + reason });
        });
    });
};

// To add new user
exports.addSingleUser = function (userBody) {
    return new Promise(function (resolve, reject) {
        var newUser1 = new usermodel1({
            primary: userBody.primary,
            userID: userBody.userID,
            userName: userBody.userName,
            userAddress: userBody.userAddress
        });
        newUser1.save().then(function () {
            resolve({ status: 201, message: 'user details added successfully', data: newUser1 });
        }).catch(function (reason) {
            reject({ status: 500, message: "Error" + reason });
        });
    });
};

//To retrieve all user details
exports.getUsers = function () {
    return new Promise(function (resolve, reject) {
        usermodel.find().exec().then(function (value) {
            resolve({ status: 200, "data": value });
        }).catch(function (reason) {
            reject({ status: 500, message: "Cannot retrieve any Details, Error : " + reason });
        })
    });
};

//To retrive user details by ID
exports.getUsersById = function (ID) {
    return new Promise(function (resolve, reject) {
        usermodel.find({userID:ID}).exec().then(function (value) {
            resolve({ status: 200, "data": value });
        }).catch(function (reason) {
            reject({ status: 500, message: "Cannot retrieve any Details, Error : " + reason });
        })
    });
};


