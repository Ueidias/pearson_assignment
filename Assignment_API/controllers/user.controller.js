const express = require('express');
const router = express.Router();
const userModel = require('../models/user.model');

router.post('/add',function (req,res) {
    userModel.addUsers(req.body).then(result=>{
        res.send(result);
    }).catch(err=>{
        res.send(err);
        console.log(err);
    })
});

router.post('/addSingleUser',function (req,res) {
    userModel.addSingleUser(req.body).then(result=>{
        res.send(result);
    }).catch(err=>{
        res.send(err);
        console.log(err);
    })
});

router.get('/',function (req,res) {
    userModel.getUsers().then(result=>{
        res.send(result);
    }).catch(err=>{
        res.send(err);
        console.log(err);
    })
});

module.exports = router;